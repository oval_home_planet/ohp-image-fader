(function($) {
    $.fn.ohp_image_fader = function(options){
        var defaults = {
            wait   : 2000,
            speed  : 800,
            easing : 'linear',
            onmouseStop : 1
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);

		var clickCancel = 0;
		var loop;

		var ul = self.children('ul');

		if ( ul.children('li').length == 1 ){
			ul.children('li:fast').clone(true).insertAfter(ul.children('li:last'));
		}
		ul.html(ul.children('li').get().reverse());

//-------------------------------

		$(window).on('load resize', function(){
			ul.css('height', ul.children('li').height());
		});

//-------------------------------

		$(window).on('load', function(){
			startLoop();
		});

//-------------------------------

		function startLoop(){
			stopLoop();

			loop = setInterval(function() {
				if ( clickCancel == 0 ){
					slideChange();
				}
			}, setting.wait+setting.speed);
		}

		function stopLoop(){
			clearInterval(loop);
		}

//-------------------------------

		function slideChange(){
			clickCancel = 1;
			stopLoop();

			ul.children('li:last').animate({
				opacity : 0
			}, {
			    duration : setting.speed,
			    easing   : setting.easing,
			    complete : function() {
				    ul.children('li:last').clone().insertBefore(ul.children('li:first'));
			    	ul.children('li:last').remove();
			    	ul.children('li:first').css('opacity', 1);
				    clickCancel = 0;
				    startLoop();
			    }
			});
		}

//-------------------------------

		$(this).mouseover(function(){
			if ( setting.onmouseStop == 1 ){
				stopLoop();
			}
		});

		$(this).mouseout(function(){
			startLoop();
		});

//-------------------------------

        return(this);
    };
})(jQuery);