(function($) {
    $.fn.ohp_image_fader = function(options){
        var defaults = {
            wait   : 2000,
            speed  : 800,
            easing : 'linear',
            zoom   : 1,
            fullScreen  : 0,
            onmouseStop : 1
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);

		var clickCancel = 0;
		var loop;
        var aspectBox;
        var aspectImage;

		var ul = self.children('ul');

		if ( ul.children('li').length == 1 ){
			ul.children('li:fast').clone(true).insertAfter(ul.children('li:last'));
		}
		ul.html(ul.children('li').get().reverse());

//-------------------------------

		$(window).on('load resize', function(){
            if ( setting.fullScreen == 1 ){
                self.height($(window).height());
            }

            ul.children('li').height(self.height());
            ul.height(self.height());

            aspectBox   = ul.width() / ul.height();
            aspectImage = ul.children('li').children('img').width() / ul.children('li').children('img').height();

            if ( aspectBox > aspectImage ){
                ul.children('li').children('img').css('width', '100%').css('height', 'auto');
            }else{
                ul.children('li').children('img').css('width', 'auto').css('height', '100%');
            }
		});

//-------------------------------

		$(window).on('load', function(){
            ul.children('li').children('img').css({transform:'scale('+setting.zoom+')'}).css({transition:'transform '+(setting.wait * 0.01)+'s'});
            ul.children('li:first').children('img').css({transform:'scale(1)'});

            ul.css('visibility','visible');

			startLoop();
		});

//-------------------------------

		function startLoop(){
			stopLoop();

			loop = setInterval(function() {
				if ( clickCancel == 0 ){
					slideChange();
				}
			}, setting.wait+setting.speed);
		}

		function stopLoop(){
			clearInterval(loop);
		}

//-------------------------------

		function slideChange(){
			clickCancel = 1;
			stopLoop();

			ul.children('li:last').animate({
				opacity : 0
			}, {
			    duration : setting.speed,
			    easing   : setting.easing,
			    complete : function() {
                    ul.children('li').children('img').css({transform:'scale('+setting.zoom+')'});
				    ul.children('li:last').clone().insertBefore(ul.children('li:first'));
			    	ul.children('li:last').remove();
			    	ul.children('li:first').css('opacity', 1).children('img').css({transform:'scale(1)'});
				    clickCancel = 0;
				    startLoop();
			    }
			});
		}

//-------------------------------

		$(this).mouseover(function(){
			if ( setting.onmouseStop == 1 ){
				stopLoop();
			}
		});

		$(this).mouseout(function(){
			startLoop();
		});

//-------------------------------

        return(this);
    };
})(jQuery);