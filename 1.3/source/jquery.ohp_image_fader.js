(function($) {
    $.fn.ohp_image_fader = function(options){
        var defaults = {
            wait        : 2000,
            speed       : 800,
            easing      : 'linear',
            bgPosition  : 'center center',
            bgSize      : 'cover',
            zoom        : 1,
            fullScreen  : 0,
            onmouseStop : 1
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);

        self.addClass('ohp-image-fader').attr('aria-live', 'polite');

		var clickCancel = 0;
		var loop;
        var aspectBox;
        var aspectImage;

		var ul = self.children('ul').attr('role', 'listbox');

		if ( ul.children('li').length == 1 ){
			ul.children('li').clone(true).insertAfter(ul.children('li'));
		}

		ul.html(ul.children('li').get().reverse());

//-------------------------------

        heightSetting();

        ul.children('li').css({'transform':'scale('+setting.zoom+')', 'transition':'transform '+(setting.wait * 0.01)+'s', 'background-position':setting.bgPosition, 'background-size':setting.bgSize}).attr('aria-hidden', 'true').attr('aria-selected', 'false');
        ul.children('li:first').css({transform:'scale(1)'}).attr('aria-hidden', 'false').attr('aria-selected', 'true');

		startLoop();

//-------------------------------

		$(window).on('resize', function(){
            heightSetting();
		});

//-------------------------------

		function startLoop(){
			stopLoop();

			loop = setInterval(function() {
				if ( clickCancel == 0 ){
					slideChange();
				}
			}, setting.wait+setting.speed);
		}

		function stopLoop(){
			clearInterval(loop);
		}

//-------------------------------

		function slideChange(){
			clickCancel = 1;
			stopLoop();

			ul.children('li:last').animate({
				opacity : 0
			}, {
			    duration : setting.speed,
			    easing   : setting.easing,
			    complete : function() {
                    ul.children('li').css({transform:'scale('+setting.zoom+')'}).attr('aria-hidden', 'true').attr('aria-selected', 'false');
				    ul.children('li:last').clone().insertBefore(ul.children('li:first'));
			    	ul.children('li:last').remove();
			    	ul.children('li:first').css('opacity', 1).css({transform:'scale(1)'}).attr('aria-hidden', 'false').attr('aria-selected', 'true');
				    clickCancel = 0;
				    startLoop();
			    }
			});
		}

//-------------------------------

        function heightSetting(){
            if ( setting.fullScreen == 1 ){
                self.height($(window).height());
            }

            ul.children('li').height(self.height());
            ul.height(self.height());
        }

//-------------------------------

		$(this).mouseover(function(){
			if ( setting.onmouseStop == 1 && setting.zoom == 1 ){
				stopLoop();
			}
		});

		$(this).mouseout(function(){
			startLoop();
		});

//-------------------------------

        return this;
    };
})(jQuery);